<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KonfirmasiPembayaran extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function pembayaran()
    {
        return $this->hasOne(Pembayaran::class, 'id', 'pembayaran_id');
    }
}

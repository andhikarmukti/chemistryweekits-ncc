<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function user()
    {
        return $this->hasOne(User::class, 'sekolah_id', 'id');
    }

    public function pesertas()
    {
        return $this->hasMany(Peserta::class, 'sekolah_id', 'id');
    }
}

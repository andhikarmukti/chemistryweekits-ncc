<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreSekolahRequest;
use App\Http\Requests\UpdateSekolahRequest;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class SekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSekolahRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Hanya peserta yang dapat submit form
        if(Gate::allows('admin')){
            return response()->json([
                'status' => 'error',
                'message' => 'hanya peserta yang dapat melakukan pengisian form'
            ], 403);
        }

        // Cegah ketika user belum melakukan submit pembayaran
        if(!auth()->user()->pembayaran){
            return response()->json([
                'status' => 'error',
                'message' => 'silahkan lakukan pengisian form pembayaran terlebih dahulu'
            ], 403);
        }

        // Hanya peserta yang sudah terverifikasi pembayaran yang bisa submit form
        if(!Gate::allows('verifikasiPembayaran')){
            return response()->json([
                'status' => 'error',
                'message' => 'Pembayaran harus dilunasi terlebih dahulu'
            ], 403);
        }

        // Validation rules
        $rules = [
            'asal_sekolah' => 'required',
            'asal_kota' => 'required',
            'asal_provinsi' => 'required',
            'guru_pendamping' => 'required',
            'no_hp_guru_pendamping' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->first()
            ], 400);
        }

        // Submit data sekolah ke db
        try{
            $sekolah = Sekolah::create($request->all());
            User::where('id', auth()->user()->id)->update([
                'sekolah_id' => $sekolah->id
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil submit data sekolah'
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sekolah  $sekolah
     * @return \Illuminate\Http\Response
     */
    public function show(Sekolah $sekolah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sekolah  $sekolah
     * @return \Illuminate\Http\Response
     */
    public function edit(Sekolah $sekolah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSekolahRequest  $request
     * @param  \App\Models\Sekolah  $sekolah
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSekolahRequest $request, Sekolah $sekolah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sekolah  $sekolah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sekolah $sekolah)
    {
        //
    }
}

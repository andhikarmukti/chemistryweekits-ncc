<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\RegisterOtp;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function fetch()
    {
        if(Gate::allows('admin')){
            $users = User::join('pembayarans', 'users.id', '=', 'pembayarans.user_id')
            ->join('konfirmasi_pembayarans', 'konfirmasi_pembayarans.pembayaran_id', '=', 'pembayarans.id')
            ->select('users.*', 'pembayarans.id as pembayaran_id', 'konfirmasi_pembayarans.status_pembayaran', 'konfirmasi_pembayarans.file_bukti_pembayaran')
            ->where('konfirmasi_pembayarans.status_pembayaran', '=', 0)
            ->with(['sekolah:id,asal_sekolah,asal_kota,asal_provinsi,guru_pendamping,no_hp_guru_pendamping', 'sekolah.pesertas:sekolah_id,id,nama_peserta,foto_peserta,kartu_pelajar_peserta,no_hp_peserta'])
            ->get()
            ->makeHidden(['created_at', 'updated_at', 'email_verified_at']);
            return response()->json([
                'status' => 'success',
                'data' => $users
            ], 200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Kamu bukan admin hey!'
            ], 403);
        }
    }

    public function fetch2()
    {
        if(Gate::allows('admin')){
            $users = User::join('pembayarans', 'users.id', '=', 'pembayarans.user_id')
            ->join('konfirmasi_pembayarans', 'konfirmasi_pembayarans.pembayaran_id', '=', 'pembayarans.id')
            ->select('users.*', 'pembayarans.id as pembayaran_id', 'konfirmasi_pembayarans.status_pembayaran', 'konfirmasi_pembayarans.file_bukti_pembayaran')
            // ->where('konfirmasi_pembayarans.status_pembayaran', '=', 0)
            ->with(['sekolah:id,asal_sekolah,asal_kota,asal_provinsi,guru_pendamping,no_hp_guru_pendamping', 'sekolah.pesertas:sekolah_id,id,nama_peserta,foto_peserta,kartu_pelajar_peserta,no_hp_peserta'])
            ->get()
            ->makeHidden(['created_at', 'updated_at', 'email_verified_at']);
            return response()->json([
                'status' => 'success',
                'data' => $users
            ], 200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Kamu bukan admin hey!'
            ], 403);
        }
    }

    private function notif($user, $jenis_lomba)
    {
        $token = 'Y1fKzTE7eDHYSuwXdcfpbww96CkexcPjSL5hMFVrEBwYMpNyfC';
        $message = 'Ada pendaftar akun baru dengan email ' . $user->email . ' untuk lomba ' . $jenis_lomba;
        $hp = '628113494546'; // '628113494546' -> no admin NCC
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://app.ruangwa.id/api/send_message',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => [
            'token' => $token,
            'number' => $hp,
            'message' => $message
        ],
        ));
        curl_exec($curl);
        curl_close($curl);

        return 'notifikasi berhasil dikirim ke nomor admin';
    }

    public function sendOtp(Request $request)
    {
        $hp_alt = Str::startsWith($request->no_hp, '0') ? '+62' . substr($request->no_hp, 1) : '0' . substr($request->no_hp, 2);
        $rules = [
            'no_hp' => 'required|string|min:8|unique:users',
            'hp_alt' => 'unique:users,no_hp',
        ];
        $validator = Validator::make(array_merge($request->all(), ['hp_alt' => $hp_alt]), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->first()
            ], 400);
        }

        $otp = rand(100000,999999);
        $token = 'Y1fKzTE7eDHYSuwXdcfpbww96CkexcPjSL5hMFVrEBwYMpNyfC';
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://app.ruangwa.id/api/send_message',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => [
            'token' => $token,
            'number' => $request->no_hp,
            'message' => 'nomor otp : ' . $otp
        ],
        ));
        curl_exec($curl);
        curl_close($curl);

        RegisterOtp::create([
            'no_hp' => $request->no_hp,
            'otp'  => $otp,
            'created_at' => now()
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil mengirim otp ke nomor ' . $request->no_hp
        ], 200);
    }

    public function checkOtp(Request $request)
    {
        $checkOtp = RegisterOtp::where([
            'no_hp' => $request->no_hp,
            'otp' => $request->otp
        ])->first();

        // Pengecekan ketika otp tidak sesuai
        if(!$checkOtp){
            return response()->json([
                'status' => 'err',
                'message' => 'otp not found!'
            ], 403);
        }else{
            // Cek expired otp selama 10 menit batas waktu
            $expiredTime = date("Y-m-d H:i:s",strtotime($checkOtp->created_at) + 600);
            if(now() > $expiredTime){
                return response()->json([
                    'status' => 'err',
                    'message' => 'otp expired!'
                ], 403);
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => 'otp ok!'
        ], 200);
    }

    public function register(Request $request)
    {
        $rules = [
            'full_name' => 'required',
            'no_hp' => 'required',
            'email' => 'required|email:dns|unique:users,email',
            'password' => 'required|min:6',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::create([
            'full_name' => $request->full_name,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $notif = $this->notif($user, 'NCC');
        return response()->json([
            'status' => 'success',
            'message' => 'Registrasi akun berhasil! ' . $notif
        ], 200);

    }

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email:dns',
            'password' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'failed',
                'message' => $validator->errors()
            ], 400);
        }

        if (!Auth::attempt($request->only('email', 'password')))
        {
            return response()->json([
                'status' => 'failed',
                'message' => 'Email atau password salah'
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken($user->id)->plainTextToken;

        return response()
            ->json([
                'status' => 'success',
                'message' => $user->name . ' Berhasil login!',
                'access_token' => $token,
                'token_type' => 'Bearer',
                'role' => $user->role,
                'user' => $user
            ]);
    }
}

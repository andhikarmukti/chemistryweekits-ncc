<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran;
use App\Http\Requests\StorePembayaranRequest;
use App\Http\Requests\UpdatePembayaranRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class PembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetch(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'data' => auth()->user()->pembayaran
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePembayaranRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Admin tidak boleh submit pembayaran
        if(Gate::allows('admin')){
            return response()->json([
                'status' => 'error',
                'message' => 'Anda adalah admin, silahkan gunakan akun peserta untuk submit pembayaran'
            ], 403);
        }

        // Validation rules
        $rules = [
            'kategori_id' => 'required',
            'region_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->first()
            ], 400);
        }

        // Menentukan nominal presale atau normal
        $nominal = 90000;
        if(strtotime(today()) <= strtotime("2022-06-18 23:59:59")){
            $nominal = 80000;
        }

        // // Pengecekan apakah user tersebut sudah melakukan submit pembayaran atau belum
        // $userSubmited = Pembayaran::where('user_id', auth()->user()->id)->first();
        // if($userSubmited != null){
        //     return response()->json([
        //         'status' => 'error',
        //         'message' => 'kamu sudah melakukan submit pembayaran, silahkan selesaikan pembayarannya terlebih dahulu'
        //     ], 200);
        // }
        // Submit data pembayaran
        try{
            $pembayaran = Pembayaran::updateOrCreate(
            [
                'user_id' => auth()->user()->id,
            ],
            [
                'user_id' => auth()->user()->id,
                'kategori_id' => $request->kategori_id,
                'region_id' => $request->region_id,
                'nominal' => $nominal + $request->region_id
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil submit pembayaran',
                'nominal' => $pembayaran->nominal
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function show(Pembayaran $pembayaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Pembayaran $pembayaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePembayaranRequest  $request
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pembayaran $pembayaran)
    {
        //
    }
}

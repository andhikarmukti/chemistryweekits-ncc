<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\SendMail;
use App\Models\Peserta;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use function PHPSTORM_META\map;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StorePesertaRequest;
use App\Http\Requests\UpdatePesertaRequest;

class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetch()
    {
        // return auth()->user()->pembayaran;
        if(Gate::allows('admin')){
            $pesertas = Peserta::all()->makeHidden(['created_at', 'updated_at']);
            return response()->json([
                'data' => $pesertas
            ], 200);
        }
        $pesertas = Peserta::where('sekolah_id', auth()->user()->sekolah_id)->get()->makeHidden(['created_at', 'updated_at']);
        return response()->json([
            'data' => $pesertas,
            'status_pembayaran' => auth()->user()->pembayaran ? auth()->user()->pembayaran->konfirmasiPembayaran->status_pembayaran : 0,
            'user' => auth()->user()
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function eKartu()
    {
        $data = User::where('id', auth()->user()->id)
        ->with(['pembayaran' => function($pembayaran){
            $pembayaran->with('region');
        }])
        ->with(['sekolah:id,asal_sekolah,asal_kota,asal_provinsi,guru_pendamping,no_hp_guru_pendamping', 'sekolah.pesertas:sekolah_id,id,nama_peserta,foto_peserta,kartu_pelajar_peserta,no_hp_peserta'])
        ->get()->makeHidden(['created_at', 'updated_at']);

        return response()->json([
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePesertaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Pencegahan double submit
        $pesertaSubmited = Peserta::where('sekolah_id', auth()->user()->sekolah_id)->first();
        if($pesertaSubmited != null){
            return response()->json([
                'status' => 'error',
                'message' => 'submit peserta hanya bisa dilakukan 1 kali'
            ], 200);
        }

        // Hanya peserta yang bisa melakukan submit form
        if(Gate::allows('admin')){
            return response()->json([
                'status' => 'error',
                'message' => 'hanya peserta yang diperbolehkan mengisi form. Anda adalah admin'
            ], 403);
        }

        // Hanya peserta yang sudah terverifikasi pembayaran yang bisa submit form
        if(!Gate::allows('verifikasiPembayaran')){
            return response()->json([
                'status' => 'error',
                'message' => 'Silahkan lakukan pembayaran terlebih dahulu'
            ], 403);
        }

        // Apakah sudah mengisi data sekolah atau belum?
        if(auth()->user()->sekolah_id == null){
            return response()->json([
                'status' => 'error',
                'message' => 'harap untuk mengisi data sekolah terlebih dahulu'
            ], 403);
        }

        // Validation rules
        $rules = [
            'nama_peserta_1' => 'required',
            'nama_peserta_2' => 'required',
            'no_hp_peserta_1' => 'required',
            'no_hp_peserta_2' => 'required',
            'foto_peserta_1' => 'required|mimes:jpeg,jpg,png|max:50000',
            'kartu_pelajar_peserta_1' => 'required|mimes:jpeg,jpg,png|max:50000',
            'foto_peserta_2' => 'required|mimes:jpeg,jpg,png|max:50000',
            'kartu_pelajar_peserta_2' => 'required|mimes:jpeg,jpg,png|max:50000',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->first()
            ], 400);
        }

        DB::beginTransaction();
        try{
            $nama_foto_peserta_1 = 'pas_photo_' . str_replace(' ', '', $request->nama_peserta_1) . '.' . $request->foto_peserta_1->extension();
            $nama_kartu_pelajar_peserta_1 = 'kartu_pelajar_' . str_replace(' ', '', $request->nama_peserta_1) . '.' . $request->kartu_pelajar_peserta_1->extension();
            Peserta::create([
                'sekolah_id' => auth()->user()->sekolah_id,
                'nama_peserta' => $request->nama_peserta_1,
                'foto_peserta' => $nama_foto_peserta_1,
                'kartu_pelajar_peserta' => $nama_kartu_pelajar_peserta_1,
                'no_hp_peserta' => $request->no_hp_peserta_1,
            ]);
            $nama_foto_peserta_2 = 'pas_photo_' . str_replace(' ', '', $request->nama_peserta_2) . '.' . $request->foto_peserta_2->extension();
            $nama_kartu_pelajar_peserta_2 = 'kartu_pelajar_' . str_replace(' ', '', $request->nama_peserta_2) . '.' . $request->kartu_pelajar_peserta_2->extension();
            Peserta::create([
                'sekolah_id' => auth()->user()->sekolah_id,
                'nama_peserta' => $request->nama_peserta_2,
                'foto_peserta' => $nama_foto_peserta_2,
                'kartu_pelajar_peserta' => $nama_kartu_pelajar_peserta_2,
                'no_hp_peserta' => $request->no_hp_peserta_2,
            ]);
            $request->foto_peserta_1->move(public_path('images'), $nama_foto_peserta_1);
            $request->kartu_pelajar_peserta_1->move(public_path('images'), $nama_kartu_pelajar_peserta_1);
            $request->foto_peserta_2->move(public_path('images'), $nama_foto_peserta_2);
            $request->kartu_pelajar_peserta_2->move(public_path('images'), $nama_kartu_pelajar_peserta_2);
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }

        // Send mail notifikasi
        $details = [
            'title' => 'NCC Chemweek ITS',
            'link_ekartu' => 'https://ncc.chemweekits.com/e-kartu',
            'link_kisi' => 'https://bit.ly/Kisi-KisiNCC2022',
            'link_latihan_soal' => 'https://bit.ly/LatihanSoalNCC2022'
        ];
        Mail::to(auth()->user()->email)->send(new SendMail($details));

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menambah data peserta'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function show(Peserta $peserta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function edit(Peserta $peserta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePesertaRequest  $request
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'nama_peserta_1' => 'required',
            'nama_peserta_2' => 'required',
            'no_hp_peserta_1' => 'required',
            'no_hp_peserta_2' => 'required',
            'foto_peserta_1' => 'required|mimes:jpeg,jpg,png|max:5120',
            'kartu_pelajar_peserta_1' => 'required|mimes:jpeg,jpg,png|max:5120',
            'foto_peserta_2' => 'required|mimes:jpeg,jpg,png|max:5120',
            'kartu_pelajar_peserta_2' => 'required|mimes:jpeg,jpg,png|max:5120',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->first()
            ], 400);
        }

        DB::beginTransaction();
        try{
            $nama_foto_peserta_1 = 'pas_photo_' . str_replace(' ', '', $request->nama_peserta_1) . '.' . $request->foto_peserta_1->extension();
            $nama_foto_peserta_2 = 'pas_photo_' . str_replace(' ', '', $request->nama_peserta_2) . '.' . $request->foto_peserta_2->extension();
            $nama_kartu_pelajar_peserta_1 = 'kartu_pelajar_' . str_replace(' ', '', $request->nama_peserta_1) . '.' . $request->kartu_pelajar_peserta_1->extension();
            $nama_kartu_pelajar_peserta_2 = 'kartu_pelajar_' . str_replace(' ', '', $request->nama_peserta_2) . '.' . $request->kartu_pelajar_peserta_2->extension();

            $peserta_1 = Peserta::where('sekolah_id', auth()->user()->sekolah_id)->get()->first();
            unlink('images/' . $peserta_1->foto_peserta);
            $request->foto_peserta_1->move(public_path('images'), $nama_foto_peserta_1);
            unlink('images/' . $peserta_1->kartu_pelajar_peserta);
            $request->kartu_pelajar_peserta_1->move(public_path('images'), $nama_kartu_pelajar_peserta_1);

            $peserta_2 = Peserta::where('sekolah_id', auth()->user()->sekolah_id)->get()->last();
            unlink('images/' . $peserta_2->foto_peserta);
            $request->foto_peserta_2->move(public_path('images'), $nama_foto_peserta_2);
            unlink('images/' . $peserta_2->kartu_pelajar_peserta);
            $request->kartu_pelajar_peserta_2->move(public_path('images'), $nama_kartu_pelajar_peserta_2);

            Peserta::where('sekolah_id', auth()->user()->sekolah_id)->get()->first()->update([
                'sekolah_id' => auth()->user()->sekolah_id,
                'nama_peserta' => $request->nama_peserta_1,
                'foto_peserta' => $nama_foto_peserta_1,
                'kartu_pelajar_peserta' => $nama_kartu_pelajar_peserta_1,
                'no_hp_peserta' => $request->no_hp_peserta_1,
            ]);
            Peserta::where('sekolah_id', auth()->user()->sekolah_id)->get()->last()->update([
                'sekolah_id' => auth()->user()->sekolah_id,
                'nama_peserta' => $request->nama_peserta_2,
                'foto_peserta' => $nama_foto_peserta_2,
                'kartu_pelajar_peserta' => $nama_kartu_pelajar_peserta_2,
                'no_hp_peserta' => $request->no_hp_peserta_2,
            ]);

            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil lock data peserta'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Peserta $peserta)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KonfirmasiPembayaran;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreKonfirmasiPembayaranRequest;
use App\Http\Requests\UpdateKonfirmasiPembayaranRequest;

class KonfirmasiPembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreKonfirmasiPembayaranRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Mencegah akses melalui user yang bukan admin
        if(Gate::allows('admin')){
            return response()->json([
                'status' => 'error',
                'message' => 'kamu adalah admin, hanya peserta yang dapat melakukan upload bukti pembayaran'
            ], 403);
        }

        // Validation rules
        $rules = [
            'pembayaran_id' => 'required',
            'nama_pemilik_rekening' => 'required',
            'bank' => 'required',
            'tanggal_transfer' => 'required',
            'file_bukti_pembayaran' => 'required|image|mimes:jpeg,png,jpg',
            'user_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->first()
            ], 400);
        }

        $bukti_pembayaran_name = 'bukti_pembayaran_' . auth()->user()->id . '.' . $request->file_bukti_pembayaran->extension();
        try{
            KonfirmasiPembayaran::updateOrCreate([
                'pembayaran_id' => $request->pembayaran_id,
            ],
            [
                'pembayaran_id' => $request->pembayaran_id,
                'nama_pemilik_rekening' => $request->nama_pemilik_rekening,
                'bank' => $request->bank,
                'tanggal_transfer' => $request->tanggal_transfer,
                'file_bukti_pembayaran' => $bukti_pembayaran_name,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }

        // Simpan gambar ke folder setelah berhasil update data ke database pembayarans
        $request->file_bukti_pembayaran->move(public_path('images'), $bukti_pembayaran_name);

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil melakukan konfirmasi pembayaran, silahkan tunggu 1x24 jam untuk mendapatkan konfirmasi dari Admin'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KonfirmasiPembayaran  $konfirmasiPembayaran
     * @return \Illuminate\Http\Response
     */
    public function show(KonfirmasiPembayaran $konfirmasiPembayaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KonfirmasiPembayaran  $konfirmasiPembayaran
     * @return \Illuminate\Http\Response
     */
    public function edit(KonfirmasiPembayaran $konfirmasiPembayaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateKonfirmasiPembayaranRequest  $request
     * @param  \App\Models\KonfirmasiPembayaran  $konfirmasiPembayaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // Pencegahan aksi yang mungkin dilakukan oleh akun peserta / non admin
        if(!Gate::allows('admin')){
            return response()->json([
                'status' => 'error',
                'message' => 'anda bukan admin!'
            ], 403);
        }

        // Validdtaion rules
        $rules = [
            'pembayaran_id' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->first()
            ], 400);
        }

        try{
            KonfirmasiPembayaran::where('pembayaran_id', $request->pembayaran_id)->update([
                'status_pembayaran' => 1
            ]);
        }catch(\Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Pembayaran telah terverifikasi'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KonfirmasiPembayaran  $konfirmasiPembayaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(KonfirmasiPembayaran $konfirmasiPembayaran)
    {
        //
    }
}

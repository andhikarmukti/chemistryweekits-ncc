<?php

namespace App\Http\Controllers;

use App\Models\RegisterOtp;
use App\Http\Requests\StoreRegisterOtpRequest;
use App\Http\Requests\UpdateRegisterOtpRequest;

class RegisterOtpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRegisterOtpRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRegisterOtpRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegisterOtp  $registerOtp
     * @return \Illuminate\Http\Response
     */
    public function show(RegisterOtp $registerOtp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegisterOtp  $registerOtp
     * @return \Illuminate\Http\Response
     */
    public function edit(RegisterOtp $registerOtp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRegisterOtpRequest  $request
     * @param  \App\Models\RegisterOtp  $registerOtp
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRegisterOtpRequest $request, RegisterOtp $registerOtp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegisterOtp  $registerOtp
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegisterOtp $registerOtp)
    {
        //
    }
}

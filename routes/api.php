<?php

use App\Models\User;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RegionController;
use App\Http\Controllers\PesertaController;
use App\Http\Controllers\SekolahController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PembayaranController;
use App\Http\Controllers\KonfirmasiPembayaranController;
use App\Models\Sekolah;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:sanctum'], function(){
    // User
    Route::get('/users', [UserController::class, 'fetch']);
    Route::get('/users2', [UserController::class, 'fetch2']);
    Route::get('/users-all', function(){
        return User::all();
    });
    //Pembayaran
    Route::post('/pembayaran', [PembayaranController::class, 'store']);
    Route::get('/pembayaran', [PembayaranController::class, 'fetch']);
    Route::post('/konfirmasi-pembayaran', [KonfirmasiPembayaranController::class, 'store']);
    Route::put('/konfirmasi-pembayaran', [KonfirmasiPembayaranController::class, 'update']);
    // Sekolah
    Route::post('/sekolah', [SekolahController::class, 'store']);
    // Peserta
    Route::post('/peserta', [PesertaController::class, 'store']);
    Route::post('/peserta-update', [PesertaController::class, 'update']);
    Route::get('/peserta', [PesertaController::class, 'fetch']);
    // Region
    Route::get('/region', [RegionController::class, 'fetch']);
    // Kategori
    Route::get('/kategori', [KategoriController::class, 'fetch']);
    // E-Kartu
    Route::get('/e-kartu', [PesertaController::class, 'eKartu']);
});

// Route::post('/register', [UserController::class, 'register']);
// Route::post('/register-send-otp', [UserController::class, 'sendOtp']);
// Route::post('/register-check-otp', [UserController::class, 'checkOtp']);
Route::post('/login', [UserController::class, 'login']);

Route::get('/userall', function(){
    return response()->json([
        'users' => User::where('sekolah_id', '!=', null)->get(),
        'sekolahs' => Sekolah::all()
    ]);
});

// Route::post('/send-mail', function(){
//     $details = [
//         'title' => 'Tes Send Email',
//         'body' => 'Tes ini adalah body'
//     ];

//     try{
//         Mail::to('andhikarmukti@gmail.com')->send(new SendMail($details));
//     }catch(\Exception $e){
//         throw $e;
//     }
//     return 'mail send!';
// });

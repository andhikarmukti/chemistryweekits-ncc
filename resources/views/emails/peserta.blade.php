<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $details['title'] }}</title>
</head>
<body>
    <h1>Hai, kamu telah berhasil melakukan pendaftaran</h1>
    <p>Silahkan klik link berikut untuk melihat e-kartu peserta</p>
    <a href="{{ $details['link_ekartu'] }}">e-kartu peserta</a><br>
    <a href="{{ $details['link_kisi'] }}">kisi kisi</a><br>
    <a href="{{ $details['link_latihan_soal'] }}">latihan soal</a>
</body>
</html>

<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSekolahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sekolahs', function (Blueprint $table) {
            $table->id();
            $table->string('asal_sekolah');
            $table->string('asal_kota');
            $table->string('asal_provinsi');
            $table->string('guru_pendamping');
            $table->string('no_hp_guru_pendamping');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE sekolahs CHANGE id id INT(4) UNSIGNED ZEROFILL AUTO_INCREMENT NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sekolahs');
    }
}

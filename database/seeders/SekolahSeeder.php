<?php

namespace Database\Seeders;

use App\Models\Sekolah;
use Illuminate\Database\Seeder;

class SekolahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sekolahs = [
            [
                'asal_sekolah' => 'SMAN 1 Cilegon',
                'asal_kota' => 'Cilegon',
                'asal_provinsi' => 'Banten',
                'guru_pendamping' => 'Roberto Wardoyo',
                'no_hp_guru_pendamping' => '08123456789',
            ],
            [
                'asal_sekolah' => 'SMAN 3 Bandung',
                'asal_kota' => 'Bandung',
                'asal_provinsi' => 'Jawa Barat',
                'guru_pendamping' => 'Sukonto Legowo',
                'no_hp_guru_pendamping' => '08157456789',
            ],
            [
                'asal_sekolah' => 'SMK 74 Jakarta',
                'asal_kota' => 'Jakarta',
                'asal_provinsi' => 'Jakarta',
                'guru_pendamping' => '-',
                'no_hp_guru_pendamping' => '-',
            ],
            [
                'asal_sekolah' => 'SMA 69 Bali',
                'asal_kota' => 'Denpasar',
                'asal_provinsi' => 'Bali',
                'guru_pendamping' => 'Made Incina',
                'no_hp_guru_pendamping' => '081346255548',
            ],
            [
                'asal_sekolah' => 'SMA 1 Serang',
                'asal_kota' => 'Serang',
                'asal_provinsi' => 'Banten',
                'guru_pendamping' => 'Sebastian Cahyono',
                'no_hp_guru_pendamping' => '081346255548',
            ],
        ];

        foreach($sekolahs as $sekolah){
            Sekolah::create([
                'asal_sekolah' => $sekolah['asal_sekolah'],
                'asal_kota' => $sekolah['asal_kota'],
                'asal_provinsi' => $sekolah['asal_provinsi'],
                'guru_pendamping' => $sekolah['guru_pendamping'],
                'no_hp_guru_pendamping' => $sekolah['no_hp_guru_pendamping'],
            ]);
        }
    }
}

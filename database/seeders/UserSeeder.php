<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'full_name' => 'Admin NCC',
                'no_hp' => '089656911145',
                'email' => 'admin@gmail.com',
                'sekolah_id' => NULL,
                'password' => Hash::make('asdasdasd')
            ],
            [
                'full_name' => 'Muhajir',
                'no_hp' => '089668311145',
                'email' => 'muhajir@gmail.com',
                'sekolah_id' => 0001,
                'password' => Hash::make('asdasdasd')
            ],
            [
                'full_name' => 'Nurul Ramadhanty',
                'no_hp' => '089799311145',
                'email' => 'nurul@gmail.com',
                'sekolah_id' => 0002,
                'password' => Hash::make('asdasdasd')
            ],
            [
                'full_name' => 'Prisilia Fani Andani',
                'no_hp' => '089668311786',
                'email' => 'prisilia@gmail.com',
                'sekolah_id' => 0003,
                'password' => Hash::make('asdasdasd')
            ],
            [
                'full_name' => 'Fadli Roby',
                'no_hp' => '089889311786',
                'email' => 'roby@gmail.com',
                'sekolah_id' => 0004,
                'password' => Hash::make('asdasdasd')
            ],
            [
                'full_name' => 'Yusril Kholili',
                'no_hp' => '089668399886',
                'email' => 'yusril@gmail.com',
                'sekolah_id' => 0005,
                'password' => Hash::make('asdasdasd')
            ],
        ];

        foreach($users as $user){
            User::create([
                'full_name' => $user['full_name'],
                'no_hp' => $user['no_hp'],
                'email' => $user['email'],
                'sekolah_id' => $user['sekolah_id'],
                'password' => $user['password']
            ]);
        }
    }
}

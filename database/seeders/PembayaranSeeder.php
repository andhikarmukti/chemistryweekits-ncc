<?php

namespace Database\Seeders;

use App\Models\Pembayaran;
use Illuminate\Database\Seeder;

class PembayaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pembayarans = [
            [
                'user_id' => 2,
                'kategori_id' => 1,
                'region_id' => 19,
                'nominal' => 80019,

            ],
            [
                'user_id' => 3,
                'kategori_id' => 1,
                'region_id' => 15,
                'nominal' => 80015,

            ],
            [
                'user_id' => 4,
                'kategori_id' => 1,
                'region_id' => 14,
                'nominal' => 90014,

            ],
            [
                'user_id' => 5,
                'kategori_id' => 1,
                'region_id' => 19,
                'nominal' => 90019,

            ],
            [
                'user_id' => 6,
                'kategori_id' => 1,
                'region_id' => 19,
                'nominal' => 80019,

            ]
        ];

        foreach($pembayarans as $pembayaran){
            Pembayaran::create([
                'user_id' => $pembayaran['user_id'],
                'kategori_id' => $pembayaran['kategori_id'],
                'region_id' => $pembayaran['region_id'],
                'nominal' => $pembayaran['nominal'],
            ]);
        }
    }
}

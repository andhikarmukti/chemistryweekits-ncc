<?php

namespace Database\Seeders;

use App\Models\Region;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            'Surabaya',
            'Mojokerto',
            'Tulungagung',
            'Kediri',
            'Malang',
            'Jember',
            'Pasuruan',
            'Banyuwangi',
            'Tuban',
            'Madiun',
            'Bangkalan',
            'Solo',
            'Kudus',
            'Jakarta',
            'Bandung',
            'Pekanbaru',
            'Balikpapan',
            'Lombok',
            'Umum',
        ];

        foreach($regions as $region){
            Region::create([
                'region' => $region
            ]);
        }
    }
}

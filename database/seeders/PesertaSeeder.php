<?php

namespace Database\Seeders;

use App\Models\Peserta;
use Illuminate\Database\Seeder;

class PesertaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pesertas = [
            [
                'sekolah_id' => 0001,
                'nama_peserta' => 'Royal Jelly',
                'foto_peserta' => 'pas_photo_RoyalJelly.PNG',
                'kartu_pelajar_peserta' => 'kartu_pelajar_RoyalJelly.jpg',
                'no_hp_peserta' => '0812345678910',
            ],
            [
                'sekolah_id' => 0001,
                'nama_peserta' => 'Andy Go To School',
                'foto_peserta' => 'pas_photo_AndyGoToSchool.PNG',
                'kartu_pelajar_peserta' => 'kartu_pelajar_AndyGoToSchool.PNG',
                'no_hp_peserta' => '089834576910',
            ],
            [
                'sekolah_id' => 0002,
                'nama_peserta' => 'Anti Dandruf',
                'foto_peserta' => 'pas_photo_AntiDandruf.PNG',
                'kartu_pelajar_peserta' => 'kartu_pelajar_AntiDandruf.jpg',
                'no_hp_peserta' => '089834575310',
            ],
            [
                'sekolah_id' => 0002,
                'nama_peserta' => 'Firman Allah',
                'foto_peserta' => 'pas_photo_FirmanAllah.jpg',
                'kartu_pelajar_peserta' => 'kartu_peserta_FrimanAllah.jpg',
                'no_hp_peserta' => '089834598610',
            ],
            [
                'sekolah_id' => 0003,
                'nama_peserta' => 'Honda Suzuki Implawati',
                'foto_peserta' => 'pas_photo_HondaSuzukiImplawati.PNG',
                'kartu_pelajar_peserta' => 'kartu_peserta_HondaSuzukiImplawati.jpg',
                'no_hp_peserta' => '089888375310',
            ],
            [
                'sekolah_id' => 0003,
                'nama_peserta' => 'Satria Baja Hitam',
                'foto_peserta' => 'pas_photo_SatriaBajaHitam.PNG',
                'kartu_pelajar_peserta' => 'kartu_peserta_SatriaBajaHitam.jpg',
                'no_hp_peserta' => '089837368610',
            ],
            [
                'sekolah_id' => 0004,
                'nama_peserta' => 'Minal Aidin Wal Faidzin',
                'foto_peserta' => 'pas_photo_MinalAidinWalFaidzin.PNG',
                'kartu_pelajar_peserta' => 'kartu_peserta_MinalAidinWalFaidzin.jpg',
                'no_hp_peserta' => '089888370980',
            ],
            [
                'sekolah_id' => 0004,
                'nama_peserta' => 'Saiton Msi',
                'foto_peserta' => 'pas_photo_SaitonMsi.PNG',
                'kartu_pelajar_peserta' => 'kartu_peserta_SaitonMSi.jpg',
                'no_hp_peserta' => '089837387910',
            ],
            [
                'sekolah_id' => 0005,
                'nama_peserta' => 'Selamet Dunia Akhirat',
                'foto_peserta' => 'pas_photo_SelametDuniaAkhirat.PNG',
                'kartu_pelajar_peserta' => 'kartu_peserta_SelametDuniaAkhirat.jpg',
                'no_hp_peserta' => '089812370980',
            ],
            [
                'sekolah_id' => 0005,
                'nama_peserta' => 'Dontworry',
                'foto_peserta' => 'pas_photo_Dontworry.PNG',
                'kartu_pelajar_peserta' => 'kartu_peserta_Dontworry.jpg',
                'no_hp_peserta' => '089837572910',
            ],
        ];

        foreach($pesertas as $peserta) {
            Peserta::create([
                'sekolah_id' => $peserta['sekolah_id'],
                'nama_peserta' => $peserta['nama_peserta'],
                'foto_peserta' => $peserta['foto_peserta'],
                'kartu_pelajar_peserta' => $peserta['kartu_pelajar_peserta'],
                'no_hp_peserta' => $peserta['no_hp_peserta'],
            ]);
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\KonfirmasiPembayaran;
use Illuminate\Database\Seeder;

class KonfirmasiPembayaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $konfirmasi_pembayarans = [
            [
                'pembayaran_id' => 1,
                'nama_pemilik_rekening' => 'Dika',
                'bank' => 'BCA',
                'tanggal_transfer' => '2022-05-22',
                'file_bukti_pembayaran' => 'bukti_pembayaran_1.jpg',
                'status_pembayaran' => 1
            ],
            [
                'pembayaran_id' => 2,
                'nama_pemilik_rekening' => 'Muhajir',
                'bank' => 'Jago',
                'tanggal_transfer' => '2022-05-24',
                'file_bukti_pembayaran' => 'bukti_pembayaran_2.jpg',
                'status_pembayaran' => 1
            ],
            [
                'pembayaran_id' => 3,
                'nama_pemilik_rekening' => 'Prisilia',
                'bank' => 'BRI',
                'tanggal_transfer' => '2022-05-28',
                'file_bukti_pembayaran' => 'bukti_pembayaran_3.jpg',
                'status_pembayaran' => 1
            ],
            [
                'pembayaran_id' => 4,
                'nama_pemilik_rekening' => 'Nurul',
                'bank' => 'Mandiri',
                'tanggal_transfer' => '2022-05-29',
                'file_bukti_pembayaran' => 'bukti_pembayaran_4.jpg',
                'status_pembayaran' => 1
            ],
            [
                'pembayaran_id' => 5,
                'nama_pemilik_rekening' => 'Febri',
                'bank' => 'BNI',
                'tanggal_transfer' => '2022-05-29',
                'file_bukti_pembayaran' => 'bukti_pembayaran_5.jpg',
                'status_pembayaran' => 1
            ],
        ];

        foreach($konfirmasi_pembayarans as $konfirmasi_pembayaran){
            KonfirmasiPembayaran::create([
                'pembayaran_id' => $konfirmasi_pembayaran['pembayaran_id'],
                'nama_pemilik_rekening' => $konfirmasi_pembayaran['nama_pemilik_rekening'],
                'bank' => $konfirmasi_pembayaran['bank'],
                'tanggal_transfer' => $konfirmasi_pembayaran['tanggal_transfer'],
                'file_bukti_pembayaran' => $konfirmasi_pembayaran['file_bukti_pembayaran'],
                'status_pembayaran' => $konfirmasi_pembayaran['status_pembayaran']
            ]);
        }
    }
}
